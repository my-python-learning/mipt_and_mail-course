from collections import OrderedDict

empty_dict = {}
empty_dict = dict()

collections_map = {
    'mutable': ['list', 'set', 'dict'],
    'immutable': ['tuple', 'frozenset']
}

print(collections_map['immutable'])

print(collections_map.get('irresistible', 'not found'))     # а-ля исключение

print('mutable' in collections_map)                         # проверка, есть ли такой ключ

beatles_map = {
    'Paul': 'Bass',
    'John': 'Guitar',
    'George': 'Guitar',
}
print(beatles_map)

beatles_map['Ringo'] = 'Drums'
print(beatles_map)

del beatles_map['John']
print(beatles_map)

beatles_map.update({
    'John': 'Guitar'
})
print(beatles_map)

print(beatles_map.pop('Ringo'))     # удаляет ключ и возвращает его значение

unknown_dict = {}
print(unknown_dict.setdefault('key', 'default'))        # проверяет, есть ли такой ключ, и если нет, то добавляет его
print(unknown_dict)

print(unknown_dict.setdefault('key', 'new_default'))
print(unknown_dict)

# итерации
print(collections_map)

for key in collections_map:
    print(key)

for key, value in collections_map.items():
    print('{} - {}'.format(key, value))

for value in collections_map.values():
    print(value)

ordered = OrderedDict()

for number in range(10):
    ordered[number] = str(number)

print(ordered)

for key in ordered:
    print(key)
