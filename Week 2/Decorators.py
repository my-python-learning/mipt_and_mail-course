import functools

def decorator(func):
    return func


@decorator
def decorated():
    print('Hello!')


def decorator(func):
    def new_func():
        pass
    return new_func


@decorator
def decorated():
    print('Hello!')


def logger(func):
    @functools.wraps(func)
    def wrapped(*args, **kwargs):
        result = func(*args, **kwargs)
        with open('log.txt', 'w') as f:
            f.write(str(result))

        return result
    return wrapped


@logger
def summator(num_list):
    return sum(num_list)


print('Summator: {}'.format(summator([1, 2, 3, 4, 5])))

with open('log.txt', 'r') as f:
    print('log.txt:  {}'.format(f.read()))

print(summator.__name__)


def logger(filename):
    def decorator(func):
        def wrapped(*args, **kwargs):
            result = func(*args, **kwargs)
            with open(filename, 'w') as f:
                f.write(str(result))
            return result
        return wrapped
    return decorator


@logger('new_log.txt')
def summator(num_list):
    return sum(num_list)


summator([1, 2, 3, 4, 5, 6])

with open('new_log.txt', 'r') as f:
    print(f.read())


def first_decorator(func):
    def wrapped():
        print('Inside first decorator product')
        return func()
    return wrapped


def second_decorator(func):
    def wrapped():
        print('Inside second decorator product')
        return func()
    return wrapped


@first_decorator
@second_decorator
def decorated():
    print('Finally called...')


decorated()

def bold(func):
    def wrapped():
        return "<b>" + func() + "</b>"
    return wrapped


def italic(func):
    def wrapped():
        return "<i>" + func() + "</i>"
    return wrapped


@bold
@italic
def hello():
    return "hello world"


# hello = bold(italic(hello))

print(hello())
