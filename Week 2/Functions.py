from datetime import  datetime


def get_seconds():
    """Return current seconds"""
    return datetime.now().second


print(get_seconds())

print(get_seconds.__doc__)


def split_tags(tag_string):
    tag_list = []
    for tag in tag_string.split(','):
        tag_list.append(tag.strip())

    return tag_list


print(split_tags('python, coursera, mooc'))


def add(x: int, y: int) -> int:
    return x + y


print(add(10, 11))
print(add('still', 'works'))


def extender(source_list, extend_list):
    source_list.extend(extend_list)


values = [1, 2, 3]
extender(values, [4, 5, 6])

print(values)


def replacer(source_tuple, replace_with):
    source_tuple = replace_with


user_info = ('Guido', '31/01')
replacer(user_info, ('Larry', '27/09'))     # неизменяемый тип не изменится энивей

print(user_info)


def say(greeting, name):
    print('{} {}!'.format(greeting, name))


say('Hello', 'Kitty')
say(name='Kitty', greeting='Hello')


def greeting(name='it\'s me...'):
    print('Hello, {}'.format(name))


greeting()


def append_one(iterable=[]):
    iterable.append(1)

    return iterable


print(append_one([1]))
print(append_one())
print(append_one())


def function(iterable=None):
    if iterable is None:
        iterable = []


def function(iterable=None):
    iterable = iterable or []


def printer(*args):
    print(type(args))

    for argument in args:
        print(argument)


printer(1, 2, 3, 4, 5)


def printer(**kwargs):
    print(type(kwargs))

    for key, value in kwargs.items():
        print('{}: {}'.format(key, value))


printer(a=10, b=11)

payload = {
    'user_id': 117,
    'feedback': {
        'subject': 'Registration fields',
        'message': 'There is no country for old men'
    }
}

printer(**payload)
