import argparse
import json
import tempfile
import os

parser = argparse.ArgumentParser()
parser.add_argument("--key")
parser.add_argument("--val")
args = parser.parse_args()

storage_path = os.path.join(tempfile.gettempdir(), 'storage.data')

try:
    with open(storage_path, 'r') as f:
        try:
            storage = json.load(f)
        except ValueError:
            storage = {}
except FileNotFoundError:
    with open(storage_path, 'tw') as f:
        storage = {}

if args.val is None:
    with open(storage_path, 'r') as f:
        if args.key in storage:
            print(', '.join(storage[args.key]))
        else:
            print(None)
else:
    with open(storage_path, 'w') as f:
        if args.key in storage:
            storage[args.key].append(args.val)
        else:
            storage[args.key] = [args.val]

        json.dump(storage, f)
