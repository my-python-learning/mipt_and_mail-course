# f = open('filename.txt')

text_modes = ['r', 'w', 'a', 'r+']
binary_modes = ['br', 'bw', 'ba', 'br+']

f = open('filename.txt', 'r+')
f.write('The world is changed.')
print(f.read())
print(f.tell())
f.seek(0)
print(f.tell())
print(f.read())
f.seek(0)
print(f.readlines())
f.close()

with open('filename.txt') as f:
    print(f.read())
