import random

# объявление списков
empty_list = []
empty_list = list()

none_list = [None] * 10

collections = ['list', 'tuple', 'dict', 'set']

user_data = [
    ['Elena', 4.4],
    ['Andrey', 4.2]
]

print(len(collections))

print(collections[0])
print(collections[-1])

collections[3] = 'frozenset'

print(collections)

range_list = list(range(10))
print(range_list[1:3])
print(range_list[::2])      # четные
print(range_list[::-1])     # реверс

for collection in collections:
    print('Learning {}...'.format(collection))

for idx, collection in enumerate(collections):
    print('#{} {}'.format(idx, collection))

collections.append('OrderedDict')       # добавление в конец списка
print(collections)

collections.extend(['ponyset', 'unicorndict'])      # добавление нового списка в конец списка
print(collections)

collections += [None]       # тоже добавление в конец списка
print(collections)

del collections[-1]     # удаление элемента списка по индексу
print(collections)

print(min(range_list))
print(max(range_list))
print(sum(range_list))

print(', '.join(collections))       # форматирование строки из элементов списка

numbers = []
for _ in range(10):
    numbers.append(random.randint(1, 20))

print(numbers)

print(sorted(numbers))
print(numbers)

numbers.sort(reverse=True)
print(numbers)

# append, clear, copy, count, extend, index, insert, pop, remove, reverse, sort

empty_tuple = ()
empty_tuple = tuple()

immutables = (int, str, tuple)      # кортежи неизменяемые
print(immutables)

blink = ([], [])        # но объекты внутри кортежа изменяемые
blink[0].append(0)
print(blink)

hash(tuple())

one_element_tuple = (1,)
print(one_element_tuple, type(one_element_tuple))
guess_what = (1)
print(guess_what, type(guess_what))
