import socket
import time


class Client:
    def __init__(self, host, port, timeout=None):
        self.host = host
        self.port = port
        self.timeout = timeout

    def get(self, metrics):
        with socket.create_connection((self.host, self.port), self.timeout) as sock:
            try:
                sock.sendall(str.encode(str.format('get {0}\n', metrics)))
                data = (sock.recv(1024)).decode()
                if "error" in data:
                    raise ClientError

                return self.__parse(data)
            except socket.timeout:
                raise ClientError

    def put(self, metrics, value, timestamp=int(time.time())):
        with socket.create_connection((self.host, self.port), self.timeout) as sock:
            try:
                sock.sendall(str.encode(str.format('put {0} {1} {2}\n', metrics, value, timestamp)))
                data = (sock.recv(1024)).decode()
                if "error" in data:
                    raise ClientError
            except socket.timeout:
                raise ClientError

    @staticmethod
    def __parse(data_set):
        data_list = []
        data_dict = {}
        for string in str(data_set).split('\n'):
            data_list.append(string.split(' '))
        data_list.remove([''])
        data_list.remove([''])
        data_list.remove(['ok'])
        for row in data_list:
            if row[0] in data_dict:
                data_dict[row[0]].append((int(row[2]), float(row[1])))
            else:
                data_dict[row[0]] = [(int(row[2]), float(row[1]))]

        return data_dict


class ClientError(Exception):
    pass


"""
client = Client("127.0.0.1", 8888, timeout=15)

client.put("palm.cpu", 0.5, timestamp=1150864247)
client.put("palm.cpu", 2.0, timestamp=1150864248)
client.put("palm.cpu", 0.5, timestamp=1150864248)

client.put("eardrum.cpu", 3, timestamp=1150864250)
client.put("eardrum.cpu", 4, timestamp=1150864251)
client.put("eardrum.memory", 4200000)

print(client.get("*"))
"""
