class User:
    def __init__(self, name, email):
        self.name = name
        self.email = email

    def get_email_data(self):
        return {
            'name': self.name,
            'email': self.email
        }

    def __str__(self):
        return '{} <{}>'.format(self.name, self.email)

    def __hash__(self):
        return hash(self.email)

    def __eq__(self, obj):
        return self.email == obj.email


jane = User('Jane Doe', 'jdoe@example.com')

print(jane.get_email_data())
print(jane)

joe = User('Joe Doe', 'jdoe@example.com')
print(jane == joe)

print(hash(jane), hash(joe))

user_email_map = {user: user.name for user in [jane, joe]}

print(user_email_map)


class Singleton:
    instance = None

    def __new__(cls):
        if cls.instance is None:
            cls.instance = super().__new__(cls)

        return cls.instance


a = Singleton()
b = Singleton()

print(a is b)


class Researcher:
    def __getattr__(self, name):  # вызывается, когда атрибута нет
        return 'Nothing found :('

    def __getattribute__(self, name):  # вызывается всегда
        print('Looking for {}'.format(name))
        return object.__getattribute__(self, name)


obj = Researcher()

print(obj.attr)
print(obj.method)
print(obj.DFG2H3J00KLL)


class Ignorant:
    def __setattr__(self, name, value):  # поведение класса при создании атрибута
        print('Not gonna set {}!'.format(name))  # сейчас он будет только выводить сообщение


obj = Ignorant()
obj.math = True
# print(obj.math)  # будет ошибка, атрибут не создастся


class Polite:
    def __delattr__(self, name):
        value = getattr(self, name)
        print(f'Goodbye {name}, you were {value}!')

        object.__delattr__(self, name)


obj = Polite()

obj.attr = 10
del obj.attr


class Logger:
    def __init__(self, filename):
        self.filename = filename

    def __call__(self, func):
        def wrapped(*args, **kwargs):
            print('call')
            with open(self.filename, 'w') as f:
                f.write('Oh Danny boy...')
            return func(*args, **kwargs)
        return wrapped


logger = Logger('log.txt')


@logger
def completely_useless_function():
    pass


completely_useless_function()

with open('log.txt') as f:
    print(f.read())


import random


class NoisyInt:
    def __init__(self, value):
        self.value = value

    def __add__(self, other):
        noise = random.uniform(-1, 1)
        return self.value + other.value + noise


a = NoisyInt(10)
b = NoisyInt(20)

for _ in range(3):
    print(a + b)


class Container:
    def __init__(self):
        self.dict = {}

    def __getitem__(self, item):
        if item in self.dict:
            print(self.dict[item])
        else:
            print('Not found...')

    def __setitem__(self, key, value):
        if key not in self.dict:
            self.dict[key] = [value, ]
        else:
            self.dict[key].append(value)


myContainer = Container()
myContainer[1]
myContainer[1] = 10
myContainer[1]


class PascalList:
    def __init__(self, original_list = None):
        self.container = original_list or []

    def __getitem__(self, index):
        return self.container[index - 1]

    def __setitem__(self, index, value):
        self.container[index - 1] = value

    def __str__(self):
        return self.container.__str__()


numbers = PascalList([1, 2, 3, 4, 5])
print(numbers[1])
