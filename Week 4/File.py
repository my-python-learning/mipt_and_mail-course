import tempfile
import os


class File:
    def __init__(self, file_path):
        self.file_path = file_path

        try:
            with open(self.file_path, 'r') as f:
                self.file_text = f.readlines()
        except FileNotFoundError:
            self.file_text = str()

        self.current_iter = 0
        self.end_iter = len(self.file_text)

    def __str__(self):
        return self.file_path

    def write(self, text):
        with open(self.file_path, 'w') as f:
            f.writelines(text)

        self.end_iter = len(self.file_text)

    def read(self):
        print(self.file_text)

    def __add__(self, other):
        if isinstance(self, type(other)):
            new_path = os.path.join(tempfile.gettempdir(), 'new_file.txt')

            new_obj = File(new_path)
            new_obj.write(self.file_text + other.file_text)

            return new_obj

    def __iter__(self):
        return self

    def __next__(self):
        if self.current_iter >= self.end_iter:
            raise StopIteration

        result = self.file_text[self.current_iter]
        self.current_iter += 1
        return result
