import csv
import os


class CarBase:
    def __init__(self, brand, photo_file_name, carrying):
        self.brand = brand
        self.photo_file_name = photo_file_name
        self.carrying = carrying

    def get_photo_file_ext(self):
        return os.path.splitext(self.photo_file_name)[1]


class Car(CarBase):

    car_type = 'car'

    def __init__(self, brand, photo_file_name, carrying, passenger_seats_count):
        super().__init__(brand, photo_file_name, carrying)
        self.passenger_seats_count = passenger_seats_count


class Truck(CarBase):

    car_type = 'truck'

    def __init__(self, brand, photo_file_name, carrying, body_whl):
        super().__init__(brand, photo_file_name, carrying)
        self.body_whl = body_whl.split('x')
        try:
            self.body_length = float(self.body_whl[0])
            self.body_width = float(self.body_whl[1])
            self.body_height = float(self.body_whl[2])
        except ValueError:
            self.body_length, self.body_width, self.body_height = 0.0, 0.0, 0.0
        finally:
            self.body_whl = [self.body_length, self.body_width, self.body_height]

    def get_body_volume(self):
        return self.body_length * self.body_width * self.body_height


class SpecMachine(CarBase):

    car_type = 'spec_machine'

    def __init__(self, brand, photo_file_name, carrying, extra):
        super().__init__(brand, photo_file_name, carrying)
        self.extra = extra


def get_car_list(csv_filename):
    car_list = []
    try:
        with open(csv_filename) as csv_fd:
            reader = csv.reader(csv_fd, delimiter=';')
            next(reader)  # пропускаем заголовок
            for row in reader:
                try:
                    if row[0] == 'car':
                        car_list.append(Car(row[1], row[3], float(row[5]), int(row[2])))
                    elif row[0] == 'truck':
                        car_list.append(Truck(row[1], row[3], float(row[5]), row[4]))
                    elif row[0] == 'spec_machine':
                        car_list.append(SpecMachine(row[1], row[3], float(row[5]), row[6]))
                except IndexError:
                    pass
                except ValueError:
                    pass
    except IOError:
        pass
    else:
        return car_list
