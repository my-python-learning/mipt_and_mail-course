class Planet:
    """This class describes planets"""

    count = 1

    def __init__(self, name, population=None):
        self.name = name
        self.population = population or []

"""
planet = Planet("Earth")
print(planet.__dict__)
planet.mass = 5.97e24
print(planet.__dict__)
print(Planet.__dict__)
print(planet.__doc__)
print(dir(planet))
print(planet.__class__)
"""

class Pet:
    def __init__(self, name=None):
        Pet._name = name or None


class Dog(Pet):
    def __init__(self, name=None):
        super().__init__(name)


print(isinstance(Dog(), Pet))
