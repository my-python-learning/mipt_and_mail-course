class Human:
    pass


class Robot:
    """Данный класс позволяет создавать роботов"""


print(Robot)


class Planet:

    def __init__(self, name):   # инициализатор
        self.name = name

    def __str__(self):   # переопределения вывода имени экземпляра
        return self.name

    def __repr__(self):   # переопределение вывода списка из экземпляров
        return f"Planet {self.name}"


"""
planet = Planet()
print(planet)
solar_system = []
for i in range(8):
    planet = Planet()
    solar_system.append(planet)

print(solar_system)

solar_system = {}
for i in range(8):
    planet = Planet()
    solar_system[planet] = True

print(solar_system)
"""

earth = Planet("Earth")
print(earth.name)
print(earth)

solar_system = []

planet_names = ["Mercury", "Venus", "Earth", "Mars",
                "Jupiter", "Saturn", "Uranus", "Neptune"]

for name in planet_names:
    planet = Planet(name)
    solar_system.append(planet)

print(solar_system)

mars = Planet("Mars")
print(mars)

print(mars.name)

mars.name = "Second Earth?"
print(mars.name)

del mars.name

# print(mars.name) ошибка AttributeError
