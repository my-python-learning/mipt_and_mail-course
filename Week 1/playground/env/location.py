# -*- coding: utf-8 -*-
"""
Created on Sat Feb  9 19:49:28 2019

@author: zapko
"""

import requests
import pprint

def get_location_info():
    return requests.get("http://ip-api.com/json/").json()

if __name__ == "__main__":
    pprint.pprint(get_location_info())