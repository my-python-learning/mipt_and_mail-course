# -*- coding: utf-8 -*-
"""
Created on Sat Feb  9 19:49:28 2019

@author: zapko
"""

import requests

def get_location_info():
    return requests.get("http://freegeoip.net/json/").json()

if __name__ == "__main__":
    print(get_location_info())