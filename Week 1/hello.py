# -*- coding: utf-8 -*-
"""
Created on Fri Feb  8 23:00:45 2019

@author: zapko
"""

# Comment
print("Привет, мир!")  # Comment

"""
Многострочный комментарий
"""
num = 100_000_000
print(num)

num = 13
print(type(num))

num = 13.4
print(num)

num = 100_000.000_001
print(num)

num = 1.5e2
print(num)

num = 150.2
print(type(num))

num = int(num)
print(num, type(num))

num = float(num)
print(num, type(num))


num = 14 + 1j

print(type(num))
print(num.real)
print(num.imag)


x1, y1 = 0, 0
x2 = 3
y2 = 4

distance = ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5
print(distance)


a = 100
b = 200
print(a, b)

a, b = b, a
print(a, b)


x = y = 0
x += 1

print(x)
print(y)

x = y = []
x.append(1)
x.append(2)

print(x)
print(y)


x = 2
print(1 < x < 3)


y = False
print(not y)

x, y, z = True, False, True
result = x and y or z
print(result)


x = 12
y = False
print(x or y) # 12

x = 12
z = "boom"
print(x and z) # boom


year = 2017
is_leap = year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)
print(is_leap)

import calendar
print(calendar.isleap(1980))


example_string = r"Файл на диске c:\\" # r-строка (сырая строка), игнорируется обратный слэш, как специальный символ
print(example_string)

example_string = "bla bla " \
"bla bla bla"
print(example_string)

example_string = """
bla bla bla
bla bla
"""
print(example_string)

# объединение строк
example_string = "Можно просто " + "складывать строки "
print(example_string)

example_string *= 3 # повторит три раза
print(example_string)

print(id(example_string)) # ячейка строки в памяти, при её изменении выделяется новая ячейка

#срезы строк
print(example_string[7:23])

example_string = "0123456789"
print(example_string[::2]) #02468

example_string = "Москва"
print(example_string[::-1]) #авксоМ

# у строк есть методы
quote = """Болтовня ничего не стоит, покажите мне код.

Linus Torvalds
"""
print(quote.count("о"))

print("москва".capitalize())

print("2017".isdigit())


print("3.14" in "Число Пи = 3.1415926")
print("Алексей" in "Александр Пушкин")


example_string = "Привет"
for letter in example_string:
    print("Буква", letter)
    
    
num_string = str(999.01)
print(type(num_string))
print(num_string)


# форматирование строки
template = "%s - главное достоинство программиста. (%s)"
print(template % ("Лень", "Larry Wall"))

print("{} не лгут, но {} пользуются формулами ({})".format("Цифры", "лжецы", "Robert A. Heinlein"))

print("{num} Кб должно хватить для любых задач. ({author})".format(num = 640, author = "Bill Gates"))

subject = "оптимизация"
author = "Donald Knuth"
print(f"Преждевременная {subject} - корень всех зол. ({author})") # f-строка

# Модификаторы форматирования
num = 8
print(f"Binary: {num:#b}")
    
num = 2 / 3
print(num)
print(f"{num:.3f}")


# байтовые строки
example_bytes = b"hello"
print(type(example_bytes))

for element in example_bytes:
    print(element)
    
# с русской кодировкой не сработает
example_string = "привет"
print(type(example_string))
print(example_string)
encoded_string = example_string.encode(encoding = "utf-8")
print(encoded_string)
print(type(encoded_string))

# декодировка байтов обратно в строку
decoded_string = encoded_string.decode()
print(decoded_string)


# объект None
answer = None
print(type(answer))

income = 0

if not income:
    print("Ничего не заработали")
    
income = None

if income is None:
    print("Ещё не начинали продавать")
elif not income:
    print("Ничего не заработали")
    
    
# Конструкции управления потоком
company = "my.com"
if "my" in company:
    print("Условие выполнено!")
    
company = "example.net"
if "my" in company or company.endswith(".net"):
    print("Условие выполнено!")
    
# тернарный оператор
score_1 = 5
score_2 = 0
winner = "Argentina" if score_1 > score_2 else "Jamaica"
print(winner)

# while
i = 0

while i < 100:
    i += 1
    
print(i)

# for
name = "Alex"
for letter in name:
    print(letter)
    
for i in range(3):
    print(i)
    
result = 0

for i in range(101):
    result += i
    
print(result)

for i in range(5, 8):
    print(i)
    
for i in range(1, 10, 2):
    print(i)
    
for i in range(10, 5, -1):
    print(i)
    
# pass - блок, который ничего не делает
for i in range(5):
   pass
    
# break
result = 0

while True:
    result += 1
    if result >= 100:
        break

print(result)

# continue
result = 0

for i in range(10):
    if i % 2 != 0:
        continue
    result += i
    
print(result)