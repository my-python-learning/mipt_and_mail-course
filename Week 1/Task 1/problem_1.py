# -*- coding: utf-8 -*-
"""
Created on Sat Feb  9 20:20:30 2019

@author: zapko
"""

import sys

digit_string = sys.argv[1]

sum = 0

for element in digit_string:
    sum += int(element)
    
print(sum)