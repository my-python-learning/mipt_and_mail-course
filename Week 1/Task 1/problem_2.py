# -*- coding: utf-8 -*-
"""
Created on Sat Feb  9 20:35:38 2019

@author: zapko
"""

import sys

num_steps = int(sys.argv[1])

for i in range(num_steps):
    print(" " * (num_steps - i - 1) + "#" * (i + 1))